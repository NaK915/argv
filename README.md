# Access Restricted Group Vaults


## About

ARGV (short for Access Restricted Group Vaults), is an Access Based resource sharing application. Using the application, users can store and share data files, only with the people they want to. With a high level of granularity in the amount of access provided to a specific user, people can control who does what with their common files.

### Levels of Access

- Level 0 - No Access
- Level 1 - View Access
- Level 2 - View, Create, Update Access
- Level 3 - View, Create, Update, Delete Access

### How the application works
[![alt-text](./design_ss/user_access.png "User Access")](https://whimsical.com/user-registration-P1ic5AdyxdmopEkuLtB5tz)

Users are allowed to access two types of spaces, Shared and Personal.
Personal spaces are limited to a single person, with full access to all its data at the topmost Level (Level 3).

Groups are spaces where people can store and collaborate on files with security. Users can define who will have access to resources in these areas.

[![alt-text](./design_ss/user_functions.png "User Access")](https://whimsical.com/user-registration-P1ic5AdyxdmopEkuLtB5tz)

Users have a large set of functions available to them, including - 
- Register
- Login
- Create new groups and add users to these groups
- Join existing groups
- View, Add, Update, Delete files (depending on access)

<!-- ### Extention to this 
ARGV will also support direct connection with [_ Shiva's repository Link here _](https://gitlab.com) for editing files. ARGV users will not need to login separately; they can directly open their files on [_ Shiva's repository Link here _](https://gitlab.com) for editing their files online and saving them as is.

*** -->

### For the project source code, go to [this](https://gitlab.com/NaK915/argv-application) repository.